function f(n){
  if(n == 0){
    return 0;
  }else{
    return 2 * f(n-1) + 1;
  }
}

function fibonacci(num){
  var a = 1, b = 0, temp;
  while (num >= 0){
    temp = a;
    a = a + b;
    b = temp;
    num--;
  }
  return b;
}

function parseData(points){
  var data = {x: [], y:[]};
  for(var i=0; i<points.length; i++){
      data.x.push( points[i][0] );
      data.y.push( points[i][1] );
  }
  console.log(data);
  return data;
}

function unparseData(arrayX, arrayY){
  var result = [];
  for(var i=0; i<arrayX.length; i++){
    result.push([arrayX[i], arrayY[i]]);
  }
  return result;
}

function fit(points){
  return regression.exponential(points);
}

function test(){
  const result = regression.exponential([[0,1],[1,1],[2,2],[3,3],[4,5],[5,8],[6,13],[7,21],[8,34],[9,55],[10,89],[11,144],[12,233],[13,377],[14,610],[15,987]]);
  console.log(result);
}